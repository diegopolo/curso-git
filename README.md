
# CURSO GIT

Buenas David.

Primeramente gracias por el curso. He quitado el miedo a git y sobre todo he repasado los comandos básicos, que si bien los uso todo los dias, no llegaba a entender muchos problemas que me pasaban... la mayoría cambios entre una rama y otra y luego hay cosas que no coinciden. Claro es que me pongo con codigo cada cierto tiempo y no me acuerdo si estaba evolucionando una rama, la master o cual y luego... problemas.

Como te dije: el curso muy bien. Todo interesante, para los desarrolladores puros tanto gitflow como ci/cd super util. Para mi, gitflow me queda grande para lo que hago y no espero usar ci/cd (ya bastante tengo con lo que hago), pero la gestion de ramas me ha venido bastante bien y auqnue las uso y realizo cambios en ramas separadas, tenia problemas con los merges... ya he aprendido! 

Hecho de menos Git LFS para los dataset (en mi caso yo los uso). He estado mirando alguno pero no terminan de convencerme (DVC, creo... data version control, tipo git) pero igual LFS esta mas integrado.

Te paso los log de lo que he hecho, con todos sus errores, typos y todo. Yo no uso GUIs perfiero consola... aprendí así git, docker y docker-compose (ahora uso portainer, alguna vez... gracias a que lo comentaste tu, pero para cosas mas puntuales, exec en el contenedor, politicas de restart que se cambian rapidito).

Me ha venido bien ademas practicar con gitlab, uso github de normal.

Un log es de git puro y duro y otro de gitflow (mas pequeño). Como veras los cambios son muy tontos 'touch file' y mierdas varias, --squash y --rebase que me daban miedito. De hecho ayer tenia un repo al que tenia que cambiar la rama master entera por otra, con el amigo google aprendí pero al menos sabia que hacía.

Si vale como feedback ahora, despues de haber procesado todo: +git -gitflow. Es decir gitflow automatiza pero si no conoces bien la base es como magia y no sabes solucionar los problemas... es una opinion...

en cuanto al ci/cd no he hecho nada... por curiosidad he mirado lo que hace falta para python (aqui poco hay que compilar) y tema de test (ni idea... me haria falta formacion (lo apunto en la encuesta))

He estado mirando el tema basandome en: https://medium.com/cubemail88/setting-gitlab-ci-cd-for-python-application-b59f1fb70efe que usa pylint, flake 8. El tema de runner no lo he pillado bien, pero aunque he intentado hacer una ejemplo tonto me piden la tarjeta de credito para validacion (toma validacion de 2 factores"!!! :) ) y paso...

Asi que lo dicho... muy bien todo. Esperando otro curso tuyo... y a escuchar republicaweb : )

Saludos



## Rama Master

- Añadido 1
- Añadido 2
- Añadido 3
- Evoluciono la rama master



## Rama Mejora1

- Un
- elefante
- se
- balanceaba

## git flow cambio
